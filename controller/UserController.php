<?php

class UserController extends Controller
{
    function __construct($db)
    {
        parent::__construct('user');
        include 'model/User.php';
        $this->model = new User($db);
    }

    /**
     * Форма логирования пользователя
     * @param $params array
     * @return mixed
     */
    function getLogin()
    {
        if (isAuthorized()) {
            //Если пользователь уже зарегистрирован
            redirect('/');
            return;
        }
        echo $this->render('login');
    }

    /**
     * Форма регистрации пользователя
     * @param $params array
     * @return mixed
     */
    function getRegister()
    {
        echo $this->render('register');
    }

    /**
     * Точка входа для операции выхода пользователя
     * @param $params array
     * @return mixed
     */
    function getLogout()
    {
        logout();
        redirect('/user/login');
        return;
    }

    function postLogin($params, $post) {
        if (isset($post['login']) && isset($post['password'])) {
            $user = $this->model->findByLogin($post['login']);
            if (isset($user) && $user['password'] == md5($post['password'])) {
                unset($user->password);
                $_SESSION['user'] = $user;
                redirect('/');
            } else {
                //To do: переделать на вывод сообщения средствами вью
                echo "Пользователь не существует или введен неправильный пароль";
            }
        } else {
            echo "Не заполнены поля логин и пароль";
            redirect('/user/login');
        }
    }

    function postRegister($params, $post) {
        if (isset($post['login']) && isset($post['password'])) {
            $user = $this->model->findByLogin($post['login']);
            if ($user===false || empty($user)) {
                $this->model->add(['login' => $post['login'], 'password'=> $post['password']]);
                //To do: переделать на вывод сообщения средствами вью
                echo "Вы успешно зарегистрированы, теперь необходимо <a href='?/user/login'>залогиниться</a>";
            } else {
                //To do: переделать на вывод сообщения средствами вью
                echo "Пользователь с таким логином уже существует.";
            }
        } else {
            //To do: переделать на вывод сообщения средствами вью
            echo "Не заполнены поля логин и пароль";
        }
    }

}