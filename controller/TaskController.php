<?php

class TaskController extends Controller
{
    protected $users = null;

	function __construct($db)
	{
	    if (!isAuthorized()) {
            redirect('/user/login');
            exit;
        }
	    parent::__construct('task');
        include 'model/Task.php';
        include 'model/User.php';
        $this->users = new User($db);
		$this->model = new Task($db);
	}

	/**
	 * Форма добавление книги
	 * @param $params array
	 * @return mixed
	 */
	function getAdd()
	{
        $users = $this->users->findAll();
		echo $this->render('add', ['users' => $users, 'current_user' => getCurrentUser()]);
	}

	/**
	 * Добавление книги
	 * @param $params array
	 * @return mixed
	 */
	function postAdd($params, $post)
	{
	    if (isAuthorized()) {
            if (isset($post['description']) && isset($post['assigned_user_id'])) {
                $idAdd = $this->model->add([
                    'description' => $post['description'],
                    'assigned_user_id' => $post['assigned_user_id'],
                    'user_id' => getCurrentUser()['id'],
                    'is_done' => 0,
                    'date_added' => date("Y-m-d H:i:s")
                ]);
                if ($idAdd) {
                    redirect('/');
                } else {
                    //To do: изменить на вью
                    echo "Что-то пошло не так<br/>";
                }
            }
        } else {
            redirect('/user/login');
        }
	}

	/**
	 * Удаление задачи
	 * @param $id
	 */
	public function getDelete($params)
	{
		if (isset($params['id']) && is_numeric($params['id'])) {
			$isDelete = $this->model->delete($params['id']);
			if ($isDelete) {
                redirect('/');
			}
		}
	}

	/**
	 * Форма редактирование данных
	 * @param $id
	 */

	public function getUpdate($params)
	{
		if (isset($params['id']) && is_numeric($params['id'])) {
            $users = $this->users->findAll();
			$task = $this->model->find($params['id']);
			echo $this->render('update', ['task' => $task, 'users' => $users]);
		}
	}

	/**
	 * Изменение состояния задачи
	 * @param $id
	 */

	public function getFinish($params)
	{
	    if (isAuthorized()) {
            if (isset($params['id']) && is_numeric($params['id'])) {
                $updateParam = ['is_done' => 1];
                $isUpdate = $this->model->update($params['id'], $updateParam);
                if ($isUpdate) {
                    redirect('/');
                }
            }
        } else {
            redirect('/user/login');
        }
	}


	/**
	 * Изменение данных о задаче
	 * @param $id
	 */

	public function postUpdate($params, $post)
	{
	    if (isAuthorized()) {
            if (isset($params['id']) && is_numeric($params['id'])) {
                $updateParam = [];
                if (isset($post['description'])) {
                    $updateParam['description'] = $post['description'];
                }
                if (isset($post['assigned_user_id'])) {
                    $updateParam['assigned_user_id'] = $post['assigned_user_id'];
                }
                $updateParam['user_id'] = getCurrentUser()['id'];
                $isUpdate = $this->model->update($params['id'], $updateParam);

                if ($isUpdate) {
                    redirect('/');
                }
            }
        } else {
	        redirect('/user/login');
        }
	}

	/**
	 * Получение всех задач
	 * @return array
	 */
	public function getList()
	{
		$tasks = $this->model->findAllForUser(getCurrentUser()['id']);
		echo $this->render('list', ['tasks' => $tasks, 'user' => getCurrentUser() ]);
	}

}

