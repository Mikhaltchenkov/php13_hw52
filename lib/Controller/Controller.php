<?php

abstract class Controller
{
    protected $model = null;
    protected $name;

    function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * Отображаем шаблон
     * @param $template
     * @param $params
     */
    protected function render($template, $params = [])
    {
        $templateDir = 'template/';
        $templateFile = $templateDir . $this->name . DIRECTORY_SEPARATOR . $template . '.html';
        if (is_file($templateFile)) {
            $loader = new Twig_Loader_Filesystem($templateDir);
            $twig = new Twig_Environment($loader, array(
                'cache' => 'template/cache',
                'auto_reload' => true
            ));
//            if (count($params) > 0) {
//                extract($params);
//            }
            return $twig->render($this->name . DIRECTORY_SEPARATOR . $template . '.html', $params);
        }
    }
}