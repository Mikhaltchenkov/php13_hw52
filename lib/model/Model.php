<?php

abstract class Model
{
    protected $db = null;

    function __construct($db)
    {
        $this->db = $db;
    }
}