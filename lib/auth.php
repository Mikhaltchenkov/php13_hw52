<?php
/**
 * Проверка пользователя, авторизован ли
 * @return bool
 */
function isAuthorized()
{
    return !empty($_SESSION['user']);
}

/**
 * Получает данные авторизованного пользователя
 * @return null
 */
function getCurrentUser()
{
    if (empty($_SESSION['user'])) {
        return null;
    }
    return $_SESSION['user'];
}

/**
 * Выполняет разлогивание
 * @return bool
 */
function logout()
{
    return session_destroy();
}