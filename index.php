<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT);
ini_set('display_errors', 1);

session_start();

require_once 'vendor/autoload.php';
require_once 'lib/auth.php';

$config = include 'config.php';

/**
 * Подключение к базе данных
 */
include 'lib/database/DataBase.php';

$db = DataBase::connect(
	$config['mysql']['host'],
	$config['mysql']['dbname'],
	$config['mysql']['user'],
	$config['mysql']['pass']
);

include 'lib/controller/Controller.php';
include 'lib/model/Model.php';
include 'lib/router/router.php';
