<?php

class User extends Model
{

	/**
	* Добавление пользователя
	* @param $params array
	* @return mixed
	*/

	function add($params)
	{
		$sth = $this->db->prepare(
			'INSERT INTO user(login, password)'
			.' VALUES(:login, :password)'
		);

		$sth->bindValue(':login', $params['login'], PDO::PARAM_STR);
		$sth->bindValue(':password', md5($params['password']), PDO::PARAM_STR);

		return $sth->execute();
	}

	/**
	 * Удаление пользователя
	* @param $id int
	* @return mixed
	*/
	function delete($id)
	{
		$sth = $this->db->prepare('DELETE FROM `user` WHERE id=:id');
		$sth->bindValue(':id', $id, PDO::PARAM_INT);
		return $sth->execute();
	}

	/**
	* @param $id int
	* @param $params array
	* @return mixed
	*/
	function update($id, $params)
	{
		if (count($params) == 0) {
			return false;
		}
		$update = [];
		foreach ($params as $param => $value) {
			$update[] = $param.'`=:'.$param;
		}
		$sth = $this->db->prepare('UPDATE `user` SET `'.implode(', `', $update).' WHERE `id`=:id');

		if (isset($params['login'])) {
			$sth->bindValue(':login', $params['login'], PDO::PARAM_INT);
		}
		if (isset($params['password'])) {
			$sth->bindValue(':password', md5($params['password']), PDO::PARAM_STR);
		}
		$sth->bindValue(':id', $id, PDO::PARAM_INT);

		return $sth->execute();
	}

	/**
	* Получение всех пользователей
	* @return array
	*/
	public function findAll()
	{
		$sth = $this->db->prepare('SELECT `id`, `login`, `password` FROM `user`;');
		if ($sth->execute()) {
			return $sth->fetchAll();
		}
		return false;
	}

	/**
	* Получение всех пользователей
	* @return array
	*/
	public function findByLogin($login)
	{
		$sth = $this->db->prepare('SELECT `id`, `login`, `password` FROM `user` WHERE login=:login;');
        $sth->bindValue(':login', $login, PDO::PARAM_INT);
		if ($sth->execute()) {
			return $sth->fetch(PDO::FETCH_ASSOC);
		}
		return false;
	}

	/**
	 * Получение одного пользователя
	 * @param $id int
	 * @return array
	 */
	public function find($id)
	{
		$sth = $this->db->prepare('SELECT `id`, `login`, `password` FROM `user` WHERE id=:id');
		$sth->bindValue(':id', $id, PDO::PARAM_INT);
		$sth->execute();
		$result = $sth->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
}

