<?php

class Task extends Model
{
	/**
	* Добавление задачи
	* @param $params array
	* @return mixed
	*/

	function add($params)
	{
		$sth = $this->db->prepare(
			'INSERT INTO task(description, user_id, assigned_user_id, is_done, date_added)'
			.' VALUES(:description, :user_id, :assigned_user_id, :is_done, :date_added)'
		);

		$sth->bindValue(':description', $params['description'], PDO::PARAM_STR);
		$sth->bindValue(':user_id', $params['user_id'], PDO::PARAM_STR);
		$sth->bindValue(':assigned_user_id', $params['assigned_user_id'], PDO::PARAM_STR);
		$sth->bindValue(':is_done', $params['is_done'], PDO::PARAM_INT);
		$sth->bindValue(':date_added', $params['date_added'], PDO::PARAM_STR);

		return $sth->execute();
	}

	/**
	 * Удаление задачи
	* @param $id int
	* @return mixed
	*/
	function delete($id)
	{
		$sth = $this->db->prepare('DELETE FROM `task` WHERE id=:id');
		$sth->bindValue(':id', $id, PDO::PARAM_INT);
		return $sth->execute();
	}

	/**
	* @param $id int
	* @param $params array
	* @return mixed
	*/
	function update($id, $params)
	{
		if (count($params) == 0) {
			return false;
		}
		$update = [];
		foreach ($params as $param => $value) {
			$update[] = $param.'`=:'.$param;
		}
		$sth = $this->db->prepare('UPDATE `task` SET `'.implode(', `', $update).' WHERE `id`=:id');

		if (isset($params['description'])) {
			$sth->bindValue(':description', $params['description'], PDO::PARAM_INT);
		}
		if (isset($params['user_id'])) {
			$sth->bindValue(':user_id', $params['user_id'], PDO::PARAM_STR);
		}
		if (isset($params['assigned_user_id'])) {
			$sth->bindValue(':assigned_user_id', $params['assigned_user_id'], PDO::PARAM_INT);
		}
		if (isset($params['is_done'])) {
			$sth->bindValue(':is_done', $params['is_done'], PDO::PARAM_STR);
		}
		$sth->bindValue(':id', $id, PDO::PARAM_INT);

		return $sth->execute();
	}

	/**
	* Получение всех задач
	* @return array
	*/
	public function findAll()
	{
		$sth = $this->db->prepare('SELECT `task`.`id` AS `id`, `description`, `user_id`, `assigned_user_id`, '.
                '`us`.`login` AS `user`, `aus`.`login` AS `assigned_user`, `is_done`, `date_added` FROM `task` '.
                'INNER JOIN `user` AS `aus` ON `task`.`assigned_user_id` = `aus`.`id` '.
                'INNER JOIN `user` AS `us` ON `task`.`user_id` = `us`.`id`');
		if ($sth->execute()) {
			return $sth->fetchAll();
		}
		return false;
	}

    /**
	* Получение всех задач для текущего пользователя
	* @return array
	*/
	public function findAllForUser($user_id)
	{
		$sth = $this->db->prepare('SELECT `task`.`id` AS `id`, `description`, `user_id`, `assigned_user_id`, '.
                '`us`.`login` AS `user`, `aus`.`login` AS `assigned_user`, `is_done`, `date_added` FROM `task` '.
                'INNER JOIN `user` AS `aus` ON `task`.`assigned_user_id` = `aus`.`id` '.
                'INNER JOIN `user` AS `us` ON `task`.`user_id` = `us`.`id` WHERE `user_id` = :user_id OR `assigned_user_id` = :user_id');
        $sth->bindValue(':user_id', $user_id, PDO::PARAM_INT);
		if ($sth->execute()) {
			return $sth->fetchAll();
		}
		return false;
	}

	/**
	 * Получение одной задачи
	 * @param $id int
	 * @return array
	 */
	public function find($id)
	{
		$sth = $this->db->prepare('SELECT `id`, `description`, `user_id`, `assigned_user_id`, `is_done`, `date_added` FROM `task` WHERE id=:id');
		$sth->bindValue(':id', $id, PDO::PARAM_INT);
		$sth->execute();
		$result = $sth->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
}

